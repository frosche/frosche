extends Button

func press() -> void:
	emit_signal("pressed")

func _on_game_over_animation_finished() -> void:
	self.visible = false
