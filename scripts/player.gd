extends Node2D

class_name Player

signal step_animation_started()
signal step_animation_finished()

@onready var M :Main= get_node("/root/Main")
@onready var sprite :AnimatedSprite2D= $AnimatedSprite2D
const SPRITE_SIZE := 256
@onready var animation_player :AnimationPlayer= $AnimationPlayer
@onready var AIM :AnimatedSprite2D= $Aim


var _player :PlayerModel= null
var _jumping := false
var _inputs_queue :Array[ModelEnums.Directions]= []
var _inputs_queue_max := 1

const CROSSHAIR_DISTANCE := 230

var _idle_animation_timeout := 3.0
var _time_since_last_idle := 0.0

func _ready() -> void:
	sprite.position = Vector2.ZERO
	sprite.animation = "idle"
	sprite.play()
	animation_player.add_animation_library("player_moves", AnimationLibrary.new())
	AIM.stop()
	AIM.visible = false

func set_player_model(player_model:PlayerModel) -> void:
	_player = player_model
	_player.connect("changed_aim_direction", Callable(self, "_on_changed_aim_direction"))
	_player.connect("started_aim", Callable(self, "_on_started_aim"))
	_player.connect("stopped_aim", Callable(self, "_on_stopped_aim"))

func _process(delta : float):
	if _player.is_alive():
		_time_since_last_idle += delta
		if _time_since_last_idle > _idle_animation_timeout and sprite.animation == "idle":
			if randf() < 0.5:
				_time_since_last_idle -= _idle_animation_timeout/2.0
			else:
				_time_since_last_idle = 0
				var r := randf()
				if r < 0.6:
					sprite.play("idle_blink")
				elif r < 0.9:
					sprite.play("idle_tilt")
				else:
					sprite.play("idle_goofy")
		
		if not _jumping and not _inputs_queue.is_empty():
			var direction :ModelEnums.Directions= _inputs_queue.pop_front()
			if _player.is_aiming():
				_player.set_aim_direction(direction)
			else:
				_player.move(direction)

func start_move_animation(to:Vector2) -> void:
	var anim_string := str(position.x)+"x"+str(position.y)+"x"+str(to.x)+"x"+str(to.y)
	if not animation_player.has_animation(anim_string):
		var animation = Animation.new()
		var track_index_x = animation.add_track(Animation.TYPE_VALUE)
		var path_x := str(self.get_path())+":position:x"
		animation.track_set_path(track_index_x, path_x)
		animation.length = 0.2
		animation.track_insert_key(track_index_x, 0.0, position.x)
		animation.track_insert_key(track_index_x, 0.2, to.x)
		var track_index_y = animation.add_track(Animation.TYPE_VALUE)
		var path_y := str(self.get_path())+":position:y"
		animation.track_set_path(track_index_y, path_y)
		animation.track_insert_key(track_index_y, 0.0, position.y)
		animation.track_insert_key(track_index_y, 0.2, to.y)
		animation_player.get_animation_library("player_moves").add_animation(anim_string, animation)
	animation_player.play("player_moves/"+anim_string)
	sprite.animation = "jump"
	_jumping = true
	_emit_step_animation_started()

func _on_animation_ended(anim_name:StringName) -> void:
	if "player_moves" in anim_name:
		_jumping = false
		_emit_step_animation_finished()

func _on_changed_aim_direction(__tile:TileModel, dir:ModelEnums.Directions) -> void:
	match(dir):
		ModelEnums.Directions.NORTH:
			AIM.position.x = 0
			AIM.position.y = -CROSSHAIR_DISTANCE
			AIM.rotation = 0
		ModelEnums.Directions.WEST:
			AIM.position.x = -CROSSHAIR_DISTANCE
			AIM.position.y = 0
			AIM.rotation = -PI/2
		ModelEnums.Directions.SOUTH:
			AIM.position.x = 0
			AIM.position.y = CROSSHAIR_DISTANCE
			AIM.rotation = PI
		ModelEnums.Directions.EAST:
			AIM.position.x = CROSSHAIR_DISTANCE
			AIM.position.y = 0
			AIM.rotation = PI/2
		_:
			breakpoint

func queue_direction(diff:ModelEnums.Directions) -> void:
	if _player.is_alive() and len(_inputs_queue) <= _inputs_queue_max:
		_inputs_queue.append(diff)

func shoot() -> void:
	if _player.is_alive():
		if _player.is_aiming():
			_player.shoot()
		else:
			_player.start_aim()

func stop_aim() -> void:
	if _player.is_aiming():
		AIM.stop()
		AIM.visible = false
		_player.stop_aim()
		sprite.animation = "netpull-1"

func scale_to(size:float) -> void:
	var new_scale :float= size/SPRITE_SIZE
	scale = Vector2(new_scale, new_scale)

func _on_sprite_animation_looped() -> void:
	if sprite.animation.begins_with("idle_"):
		sprite.animation = "idle"
	elif sprite.animation == "jump":
		sprite.animation = "idle"
	elif sprite.animation == "netpull":
		sprite.animation = "netaim"
	elif sprite.animation == "netpull-1":
		sprite.position = Vector2.ZERO
		sprite.animation = "idle"

func _on_game_over_animation(_won:bool, _bomb:bool) -> void:
	_inputs_queue.clear()
	self.visible = false

func _emit_step_animation_started() -> void:
	emit_signal("step_animation_started")

func _emit_step_animation_finished() -> void:
	emit_signal("step_animation_finished")

func _on_started_aim(__tile:TileModel, _direction:ModelEnums.Directions) -> void:
	AIM.play()
	AIM.visible = true
	sprite.position.y = -SPRITE_SIZE/8.0
	sprite.animation = "netpull"

func _on_stopped_aim(__tile:TileModel, _direction:ModelEnums.Directions) -> void:
	AIM.stop()
	AIM.visible = false
