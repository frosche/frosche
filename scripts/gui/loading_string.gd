extends Label


func _on_splash_screen_loaded_stage(_stage:int, _num_stages:int, next_load_string:String) -> void:
	text = next_load_string


func _on_splash_screen_loading_finished() -> void:
	visible = false
