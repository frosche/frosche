extends Control

@onready var M :Main= get_node("/root/Main")

@onready var WEST_BUTTON := $ScreenGame/west_button
@onready var EAST_BUTTON := $ScreenGame/east_button
@onready var NORTH_BUTTON := $ScreenGame/north_button
@onready var SOUTH_BUTTON := $ScreenGame/south_button
@onready var SHOOT_BUTTON := $ScreenGame/shoot_button
@onready var CANCEL_BUTTON := $ScreenGame/cancel_button
@onready var OK_BUTTON := $ScreenGame/ok_button

enum States {
	TOUCH_TEXT,
	TOUCH_1,
	TOUCH_2,
	TOUCH_3,
	RIGHT,
	UP,
	RIGHT_2,
	ALMOST_BOMB,
	ALMOST_BOMB_2,
	BOMB,
	FREE,
	CATCH_1,
	CATCH_2
}

var _state :States= States.TOUCH_TEXT
var _cooldown := 0.0

var _highlighted :Array[Control]= []
var _highlight := 1.0
var _highlight_up := false

var _allow_jump_west := false
var _allow_jump_north := false
var _allow_jump_east := false
var _allow_jump_south := false
var _allow_shoot := false

var _game_model : GameModel

func _on_pre_game_start(game_model) -> void:
	_game_model = game_model
	_game_model.set_tutorial(true)
	_game_model.connect("game_over", Callable(self, "_on_game_over"))
	_game_model.get_player().connect("stepped", Callable(self, "_on_player_stepped"))
	_game_model.get_player().connect("started_aim", Callable(self, "_on_player_started_aim"))
	
func _ready() -> void:
	$ScreenGame.set_physics_process(false)
	WEST_BUTTON.disconnect("pressed", Callable($ScreenGame, "_on_west_button_pressed"))
	WEST_BUTTON.connect("pressed", Callable(self, "_on_west_button_pressed"))
	EAST_BUTTON.disconnect("pressed", Callable($ScreenGame, "_on_east_button_pressed"))
	EAST_BUTTON.connect("pressed", Callable(self, "_on_east_button_pressed"))
	NORTH_BUTTON.disconnect("pressed", Callable($ScreenGame, "_on_north_button_pressed"))
	NORTH_BUTTON.connect("pressed", Callable(self, "_on_north_button_pressed"))
	SOUTH_BUTTON.disconnect("pressed", Callable($ScreenGame, "_on_south_button_pressed"))
	SOUTH_BUTTON.connect("pressed", Callable(self, "_on_south_button_pressed"))
	SHOOT_BUTTON.disconnect("pressed", Callable($ScreenGame, "_on_shoot_button_pressed"))
	SHOOT_BUTTON.connect("pressed", Callable(self, "_on_shoot_button_pressed"))
	CANCEL_BUTTON.disconnect("pressed", Callable($ScreenGame, "_on_cancel_button_pressed"))
	$ScreenGame/timer_label.visible = false
	SHOOT_BUTTON.visible = false
	$ScreenGame/seed_label.visible = false
	$ScreenGame/Player._inputs_queue_max = 0

func _physics_process(delta) -> void:
	_cooldown -= delta
	if _highlight_up:
		_highlight += delta
		if _highlight > 1:
			_highlight = 1
			_highlight_up = false
	else:
		_highlight -= delta
		if _highlight < 0.6:
			_highlight = 0.6
			_highlight_up = true
	for highlighted in _highlighted:
		highlighted.modulate = Color(_highlight, _highlight, _highlight)
	if $ScreenGame._game_model.is_game_ongoing():
		if Input.is_action_just_pressed("shoot"):
			SHOOT_BUTTON.press()
		if Input.is_action_just_pressed("go_north"):
			NORTH_BUTTON.press()
		if Input.is_action_just_pressed("go_west"):
			WEST_BUTTON.press()
		if Input.is_action_just_pressed("go_south"):
			SOUTH_BUTTON.press()
		if Input.is_action_just_pressed("go_east"):
			EAST_BUTTON.press()
	else:
		if not $ScreenGame._game_over_animation_ongoing and Input.is_action_just_pressed("shoot"):
			OK_BUTTON.press()

func _input(event) -> void:
	if _cooldown <= 0.0 \
			and (event is InputEventScreenTouch \
			or event is InputEventKey \
			or event is InputEventMouseButton):
		_cooldown = 0.5
		match _state:
			States.TOUCH_TEXT:
				if (event is InputEventScreenTouch or event is InputEventMouseButton) and $text.get_rect().has_point(event.position):
					_state = States.TOUCH_1
					$text.text = "Now you may touch anywhere!"
			States.TOUCH_1:
				_state = States.TOUCH_2
				$text.text = "Welcome to Phrogg!"
			States.TOUCH_2:
				_state = States.TOUCH_3
				$text.text = "Your goal in this game is to catch the fly!"
			States.TOUCH_3:
				_state = States.RIGHT
				_add_highlight(EAST_BUTTON)
				$text.text = "To go to the right, press the right button at the bottom of the screen!"
				$ColorRect.visible = false
				_clear_allow_jumps()
				_allow_jump_east = true
			_:
				_cooldown = 0.0

func _clear_highlights() -> void:
	for node in _highlighted:
		node.modulate = Color(1,1,1)
	_highlighted.clear()

func _add_highlight(node:Control) -> void:
	_highlighted.push_back(node)

func _remove_highlight(node:Node2D) -> void:
	var index := _highlighted.find(node)
	if index != -1:
		_highlighted.pop_at(index)
		node.modulate = Color(1,1,1)

func _clear_allow_jumps() -> void:
	_allow_jump_west = false
	_allow_jump_north = false
	_allow_jump_east = false
	_allow_jump_south = false

func _do_free_allow_jump() -> String:
	var text := "Explore!"
	var tile := _game_model.get_player().get_tile()
	if tile.is_bomb_close():
		_clear_highlights()
		_clear_allow_jumps()
		if tile.get_north()[0].is_discovered():
			_add_highlight(NORTH_BUTTON)
			_allow_jump_north = true
		if tile.get_south()[0].is_discovered():
			_add_highlight(SOUTH_BUTTON)
			_allow_jump_south = true
		if tile.get_east()[0].is_discovered():
			_add_highlight(EAST_BUTTON)
			_allow_jump_east = true
		if tile.get_west()[0].is_discovered():
			_add_highlight(WEST_BUTTON)
			_allow_jump_west = true
	elif tile.is_beast_close():
		text = "You can feel the fly is no more than 2 jumps from you"
	else:
		_clear_highlights()
		_add_highlight(NORTH_BUTTON)
		_add_highlight(EAST_BUTTON)
		_add_highlight(SOUTH_BUTTON)
		_add_highlight(WEST_BUTTON)
		_allow_jump_west = true
		_allow_jump_north = true
		_allow_jump_east = true
		_allow_jump_south = true
	return text

func _on_player_stepped(_path:Array[TileModel]) -> void:
	var text := ""
	match _state:
		States.RIGHT:
			text = "Now jump up!"
			_state = States.UP
			_clear_highlights()
			_add_highlight(NORTH_BUTTON)
			_clear_allow_jumps()
			_allow_jump_north = true
		States.UP:
			text = "Seems this lilypad was a little further away!"
			_state = States.RIGHT_2
			_clear_highlights()
			_add_highlight(EAST_BUTTON)
			_clear_allow_jumps()
			_allow_jump_east = true
		States.RIGHT_2:
			text = "This is a small pond. If you go off the right side, you end up on the left. Try it out."
			_state = States.ALMOST_BOMB
			_clear_allow_jumps()
			_allow_jump_east = true
		States.ALMOST_BOMB:
			text = "Jump again"
			_state = States.ALMOST_BOMB_2
			_clear_highlights()
			_add_highlight(NORTH_BUTTON)
			_add_highlight(EAST_BUTTON)
			_add_highlight(SOUTH_BUTTON)
			_clear_allow_jumps()
			_allow_jump_north = true
			_allow_jump_east = true
			_allow_jump_south = true
		States.ALMOST_BOMB_2:
			text = "The Exclamation mark means one of the lilypads next to this one is broken! Go back!"
			_state = States.FREE
			_do_free_allow_jump()
		States.FREE:
			text = _do_free_allow_jump()
	$text.text = text

func _check_jump_fly(dir:ModelEnums.Directions) -> bool:
	match _state:
		States.FREE:
			var tile := _game_model.get_player().get_tile()
			if tile.get_direction(dir)[-1].is_beast():
				_state = States.CATCH_1
				$text.text = "You think the fly might be there. Ready your net!"
				_allow_shoot = true
				SHOOT_BUTTON.visible = true
				_clear_highlights()
				_clear_allow_jumps()
				_add_highlight(SHOOT_BUTTON)
				return true
		States.CATCH_2:
			var tile := _game_model.get_player().get_tile()
			_clear_highlights()
			if tile.get_direction(dir)[-1].is_beast():
				_add_highlight(SHOOT_BUTTON)
			else:
				_add_highlight(NORTH_BUTTON)
				_add_highlight(EAST_BUTTON)
				_add_highlight(SOUTH_BUTTON)
				_add_highlight(WEST_BUTTON)
	return false

func _check_aim_fly() -> bool:
	var player := _game_model.get_player()
	if not player.is_aiming():
		_state = States.CATCH_2
		$text.text = "Now aim and catch the fly!"
		_clear_highlights()
		_add_highlight(NORTH_BUTTON)
		_add_highlight(EAST_BUTTON)
		_add_highlight(SOUTH_BUTTON)
		_add_highlight(WEST_BUTTON)
		_allow_jump_north = true
		_allow_jump_east = true
		_allow_jump_west = true
		_allow_jump_south = true
		return true
	
	var tile := player.get_tile().get_direction(player.get_aim_direction())[-1]
	if tile.is_beast():
		$text.visible = false
		return true
	return false
	

func _on_north_button_pressed() -> void:
	if _allow_jump_north and not _check_jump_fly(ModelEnums.Directions.NORTH):
		$ScreenGame._on_north_button_pressed()

func _on_west_button_pressed() -> void:
	if _allow_jump_west and not _check_jump_fly(ModelEnums.Directions.WEST):
		$ScreenGame._on_west_button_pressed()

func _on_south_button_pressed() -> void:
	if _allow_jump_south and not _check_jump_fly(ModelEnums.Directions.SOUTH):
		$ScreenGame._on_south_button_pressed()

func _on_east_button_pressed() -> void:
	if _allow_jump_east and not _check_jump_fly(ModelEnums.Directions.EAST):
		$ScreenGame._on_east_button_pressed()

func _on_shoot_button_pressed() -> void:
	if _allow_shoot and _check_aim_fly():
		$ScreenGame._on_shoot_button_pressed()

func _on_player_started_aim(tile:TileModel, dir:ModelEnums.Directions) -> void:
	if tile.get_direction(dir)[-1].is_beast():
		_game_model.get_player().set_aim_direction(ModelEnums.opposite_direction(dir))

func _on_game_over(won:bool, _bomb:bool, _time:int, _difficulty:ModelEnums.Difficulties, _fog:bool, _tiles:Array[TileModel]) -> void:
	if won:
		M._persist.set_tutorial_done(true)
