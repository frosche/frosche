extends AudioStreamPlayer

class_name CustomAudioPlayer


var _base_volume := 0.0


func _ready() -> void:
	_base_volume = volume_db


func set_volume(volume:float) -> void:
	if volume == 0:
		volume_db = -80
	else:
		volume_db = -20*(1-volume) + _base_volume
