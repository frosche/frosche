extends Control

@onready var M :Main= get_node("/root/Main")


func _ready() -> void:
	var ret := $debug_button.connect("pressed", Callable(M, "_on_debug_pressed"))
	assert(!ret)
	ret = $music_slider.connect("value_changed", Callable(M, "_on_music_slider_value_changed"))
	assert(!ret)
	ret = $ambient_slider.connect("value_changed", Callable(M, "_on_ambient_slider_value_changed"))
	assert(!ret)
	ret = $effects_slider.connect("value_changed", Callable(M, "_on_effects_slider_value_changed"))
	assert(!ret)
	
	$debug_button.button_pressed = M.is_debug()
	
	$music_slider.value = M.get_music_volume()
	$ambient_slider.value = M.get_ambient_volume()
	$effects_slider.value = M.get_effects_volume()


func _notification(what) -> void:
	if what == NOTIFICATION_WM_GO_BACK_REQUEST:
		M.change_scene(Enums.Scenes.MAIN_MENU)


func _on_back_button_pressed() -> void:
	M.change_scene(Enums.Scenes.MAIN_MENU)


func _on_privacy_button_pressed() -> void:
	OS.shell_open("https://wurmcw.ddns.net/Tyoda/Phrogg/privacypolicy.html")


func _on_tutorial_button_pressed() -> void:
	M.change_scene(Enums.Scenes.TUTORIAL)
