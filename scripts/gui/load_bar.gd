extends ProgressBar



func _on_splash_screen_loaded_stage(stage:int, num_stages:int, _next_loading_string:String) -> void:
	value = stage
	max_value = num_stages


func _on_splash_screen_loading_finished() -> void:
	visible = false
