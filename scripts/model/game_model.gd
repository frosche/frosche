class_name GameModel

signal game_time_passed(time:float, delta:float)
signal game_started(tiles:Array[TileModel], player:PlayerModel)
signal game_over(won:bool, bomb:bool, time:int, difficulty:ModelEnums.Difficulties, fog:bool, tiles:Array[TileModel])

const CHANCES :Array[Array]= [
	[30, 33],
	[22, 28],
	[19, 26],
	[12, 21]
]

const BOMBS :Array[Array]= [
	[1],
	[2],
	[2,3],
	[1,2,3,4]
]

var _random := RandomNumberGenerator.new()
var _new_random_seed := 0

var _player := PlayerModel.new()
var _tiles :Array[TileModel]= []
var _width := 5
var _new_width := _width
var _height := 7
var _new_height := _height
var _difficulty := ModelEnums.Difficulties.TRAINEE
var _new_difficulty := _difficulty
var _bad_visibility := false
var _new_bad_visibility := _bad_visibility
var _bad_visibility_time := 5.0
var _new_bad_visibility_time := _bad_visibility_time
var _is_tutorial := false
var _new_is_tutorial := _is_tutorial

var _game_ongoing := false
var _game_won := false
var _game_time := 0.0

func start_game() -> void:
	# initializes the map and starts the game
	_random.seed = _new_random_seed
	_width = _new_width
	_height = _new_height
	_difficulty = _new_difficulty
	_bad_visibility = _new_bad_visibility
	_bad_visibility_time = _new_bad_visibility_time
	_is_tutorial = _new_is_tutorial
	_game_time = 0
	_game_ongoing = true

	var starter_tile :TileModel= null
	if _is_tutorial:
		var tiles := _generate_map_tutorial()
		tiles[0].beastify()
		tiles[1].bombify()
		starter_tile = tiles[2]
	else:
		var done := false
		while not done:
			_generate_map(_difficulty)
			var beast_tile := get_empty_tile()
			
			if beast_tile == null:
				continue
			
			beast_tile.beastify()
			
			var num_bombs :int= BOMBS[_difficulty][_random.randi()%len(BOMBS[_difficulty])]
			
			for _i in range(num_bombs):
				var bomb_tile := get_empty_tile()
				if bomb_tile == null:
					continue
				bomb_tile.bombify()
			
			starter_tile = get_very_empty_tile()
			if starter_tile == null:
				continue
			
			done = true
	starter_tile.set_has_player(true)
	_player.set_tile(starter_tile)
	_player.connect("shot", Callable(self, "_on_player_shot"))
	_player.connect("stepped", Callable(self, "_on_player_stepped"))
	
	_emit_game_started(_tiles, _player)

func get_tile_dir(x: int, y: int, d: ModelEnums.Directions) -> TileModel:
	assert(x >= 0, "x was less than zero")
	assert(y >= 0, "y was less than zero")
	assert(x < _width, "x was greater than board width")
	assert(y < _height,"y was greater than board height")
	if not _game_ongoing:
		return null
	
	for tile in _tiles:
		if tile.get_x() == x and tile.get_y() == y and tile.has_direction(d):
			return tile
	breakpoint
	return null

func _generate_map_tutorial() -> Array[TileModel]:
	# Generates the same tutorial map.
	# Returns 3 tiles, the beast, a bomb, and the starter tile.
	const tutorial_width := 5
	const tutorial_height := 7
	# x, y pos for objects, arrow tiles
	const fly_pos := [4, 1]
	const bomb_pos := [1, 3]
	const player_pos := [2, 6]
	const arrows_1_pos := [3, 5]
	const arrows_2_pos := [0, 5]
	const arrows_3_pos := [1, 5]
	_width = tutorial_width
	_height = tutorial_height
	
	var types :Array[ModelEnums.Tiles]= []
	for i in range(tutorial_height*tutorial_width):
		types.append(ModelEnums.Tiles.NORMAL)
	types[_width*_height-1 - (arrows_1_pos[1]*_width + arrows_1_pos[0])] = ModelEnums.Tiles.S_N
	types[_width*_height-1 - (arrows_2_pos[1]*_width + arrows_2_pos[0])] = ModelEnums.Tiles.S_W
	types[_width*_height-1 - (arrows_3_pos[1]*_width + arrows_3_pos[0])] = ModelEnums.Tiles.S_E
	_generate_map_tiles_from_types(types)
	var fly_tile := get_tile_dir(fly_pos[0], fly_pos[1], ModelEnums.Directions.NORTH)
	var bomb_tile := get_tile_dir(bomb_pos[0], bomb_pos[1], ModelEnums.Directions.NORTH)
	var player_tile := get_tile_dir(player_pos[0], player_pos[1], ModelEnums.Directions.NORTH)
	return [fly_tile, bomb_tile, player_tile]

func _generate_map(difficulty : ModelEnums.Difficulties) -> void:
	var types :Array[ModelEnums.Tiles]= []
	
	var num := int(_random.randf_range(CHANCES[difficulty][0], CHANCES[difficulty][1]))
	
	for _i in range(num):
		types.append(ModelEnums.Tiles.NORMAL)
	for _i in range(_width*_height - num):
		types.append(ModelEnums.Tiles.NONE)
	
	var shuffled_types :Array[ModelEnums.Tiles]= []
	while not types.is_empty():
		shuffled_types.append(types.pop_at(_random.randi() % len(types)))
	_generate_map_tiles_from_types(shuffled_types)

func _generate_map_tiles_from_types(types:Array[ModelEnums.Tiles]) -> void:
	_tiles = []
	for y in range(_height):
		for x in range(_width):
			var type :ModelEnums.Tiles= types.pop_back()
			var tile_1 := TileModel.new()
			var tile_1_type := type
			var tile_2 := TileModel.new()
			var tile_2_type := ModelEnums.Tiles.NONE
			match type:
				ModelEnums.Tiles.NORMAL:
					pass
				ModelEnums.Tiles.S_N:
					tile_2_type = ModelEnums.Tiles.W_E
				ModelEnums.Tiles.S_E:
					tile_2_type = ModelEnums.Tiles.N_W
				ModelEnums.Tiles.S_W:
					tile_2_type = ModelEnums.Tiles.N_E
				_:
					var f := _random.randf()
					if f < 0.5:
						tile_1_type = ModelEnums.Tiles.S_N
						tile_2_type = ModelEnums.Tiles.W_E
					else:
						f = _random.randf()
						if f < 0.5:
							tile_1_type = ModelEnums.Tiles.S_E
							tile_2_type = ModelEnums.Tiles.N_W
						else:
							tile_1_type = ModelEnums.Tiles.S_W
							tile_2_type = ModelEnums.Tiles.N_E
			tile_1.set_pos(x, y)
			tile_1.set_type(tile_1_type)
			_tiles.append(tile_1)
			if tile_2_type != ModelEnums.Tiles.NONE:
				tile_2.set_pos(x, y)
				tile_2.set_type(tile_2_type)
				_tiles.append(tile_2)
	
	for tile in _tiles:
		var x := tile.get_x()
		var y := tile.get_y()
		var tile_n := get_tile_dir(x, posmod(y-1, _height), ModelEnums.Directions.SOUTH)
		var tile_w := get_tile_dir(posmod(x-1, _width), y,  ModelEnums.Directions.EAST)
		var tile_s := get_tile_dir(x, (y+1)%_height, ModelEnums.Directions.NORTH)
		var tile_e := get_tile_dir((x+1)%_width, y,  ModelEnums.Directions.WEST)
		assert(tile_n != null)
		assert(tile_w != null)
		assert(tile_s != null)
		assert(tile_e != null)
		
		if tile._con_n != null:
			tile._con_n.outside = tile_n._con_s
		if tile._con_s != null:
			tile._con_s.outside = tile_s._con_n
		if tile._con_w != null:
			tile._con_w.outside = tile_w._con_e
		if tile._con_e != null:
			tile._con_e.outside = tile_e._con_w
		
		tile.set_bad_visibility(_bad_visibility)
		tile.set_bad_visibility_time(_bad_visibility_time)

func tick(delta:float) -> void:
	# Should be called every frame
	if not _game_ongoing:
		return
	
	_game_time += delta
	_player.tick(delta)
	_emit_game_time_passed(_game_time, delta)

func _on_player_shot(_from:TileModel, to:TileModel, _direction:ModelEnums.Directions) -> void:
	var won := to.is_beast()
	_game_won = won
	_game_over(false)

func set_seed(new_seed:int) -> void:
	# Set the seed to be used on new game creation
	_new_random_seed = new_seed

func get_seed() -> int:
	# Get the seed used in the current ongoing game
	return _random.seed

func get_player() -> PlayerModel:
	# Get the player
	return _player

func set_size(width:int, height:int) -> void:
	# Set the board size to be used on new game creation
	self._new_width = width
	self._new_height = height

func get_width() -> int:
	# Get the width of the current ongoing game
	return _width

func get_height() -> int:
	# Get the height of the current ongoing game
	return _height

func set_difficulty(difficulty:ModelEnums.Difficulties) -> void:
	# Set the difficulty to be used on new game creation
	self._new_difficulty = difficulty

func get_difficulty() -> ModelEnums.Difficulties:
	# Get the difficulty of the current ongoing game
	return _difficulty

func get_very_empty_tile() -> TileModel:
	if not _game_ongoing:
		return null
	
	var tile := _tiles[_random.randi()%_tiles.size()]
	var tries := 0
	while tries < 1000 and (tile.get_type() != ModelEnums.Tiles.NORMAL or tile.is_bomb() or tile.is_beast() or tile.has_player() or tile.is_bomb_close() or tile.is_beast_close()):
		tile = _tiles[_random.randi()%len(_tiles)]
		tries += 1
	if tries == 1000:
		tile = null
	return tile

func get_empty_tile() -> TileModel:
	if not _game_ongoing:
		return null
	
	var tile := _tiles[_random.randi()%_tiles.size()]
	var tries := 0
	while tries < 1000 and (tile.get_type() != ModelEnums.Tiles.NORMAL or tile.is_bomb() or tile.is_beast() or tile.has_player()):
		tile = _tiles[_random.randi()%len(_tiles)]
		tries += 1
	if tries == 1000:
		tile = null
	return tile

func _emit_game_started(tiles:Array[TileModel], player:PlayerModel) -> void:
	emit_signal("game_started", tiles, player)

func _game_over(bomb:bool) -> void:
	_game_ongoing = false
	_player.set_alive(false)
	
	_emit_game_over(_game_won, bomb, int(_game_time), _difficulty, _bad_visibility, _tiles)

func _emit_game_over(won:bool, bomb:bool, time:int, difficulty:ModelEnums.Difficulties, fog:bool, tiles:Array[TileModel]) -> void:
	emit_signal("game_over", won, bomb, time, difficulty, fog, tiles)

func _emit_game_time_passed(time:float, delta:float) -> void:
	emit_signal("game_time_passed", time, delta)

func is_game_ongoing() -> bool:
	return _game_ongoing

func _on_player_changed_shoot_direction(tile:Tile, direction:ModelEnums.Directions) -> void:
	emit_signal("player_aim_change_direction", tile, direction)

func _on_player_stepped(path:Array[TileModel]) -> void:
	var tile := path[-1]
	if tile.is_bomb() or tile.is_beast():
		_game_over(tile.is_bomb())

func set_bad_visibility(bad_visibility:bool) -> void:
	_new_bad_visibility = bad_visibility

func set_bad_visibility_time(bad_visibility_time:float) -> void:
	_new_bad_visibility_time = bad_visibility_time

func set_tutorial(is_tutorial:bool) -> void:
	_new_is_tutorial = is_tutorial
