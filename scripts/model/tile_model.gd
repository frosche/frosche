class_name TileModel

class Connector:
	var parent : TileModel
	var inside : Connector
	var outside : Connector
	
	func _init(_parent) -> void:
		parent = _parent

signal tile_state_changed()

var _y := 0
var _x := 0
var _type := ModelEnums.Tiles.NORMAL
var _discovered := false
var _visible_until := Time.get_unix_time_from_system()
var _is_bomb := false
var _is_bomb_close := false
var _is_beast := false
var _is_beast_close := false
var _always_visible := false

var _has_player := false

# store game model instead ???
var _bad_visibility := false
var _bad_visibility_time := 5.0

var _con_n :Connector= null
var _con_w :Connector= null
var _con_s :Connector= null
var _con_e :Connector= null

func set_always_visible(always_visible) -> void:
	_always_visible = always_visible
	_emit_tile_state_changed()

func set_bad_visibility(bad_visibility:float) -> void:
	_bad_visibility = bad_visibility
	_emit_tile_state_changed()

func set_bad_visibility_time(bad_visibility_time:float) -> void:
	_bad_visibility_time = bad_visibility_time
	_emit_tile_state_changed()

func set_pos(x:int, y:int) -> void:
	_x = x
	_y = y

func set_type(type:ModelEnums.Tiles) -> void:
	_type = type
	
	match _type:
		ModelEnums.Tiles.NORMAL:
			_con_n = Connector.new(self)
			_con_s = Connector.new(self)
			_con_w = Connector.new(self)
			_con_e = Connector.new(self)
		ModelEnums.Tiles.S_N:
			_con_s = Connector.new(self)
			_con_n = Connector.new(self)
			_con_s.inside = _con_n
			_con_n.inside = _con_s
		ModelEnums.Tiles.W_E:
			_con_w = Connector.new(self)
			_con_e = Connector.new(self)
			_con_w.inside = _con_e
			_con_e.inside = _con_w
		ModelEnums.Tiles.S_E:
			_con_s = Connector.new(self)
			_con_e = Connector.new(self)
			_con_s.inside = _con_e
			_con_e.inside = _con_s
		ModelEnums.Tiles.S_W:
			_con_s = Connector.new(self)
			_con_w = Connector.new(self)
			_con_s.inside = _con_w
			_con_w.inside = _con_s
		ModelEnums.Tiles.N_E:
			_con_n = Connector.new(self)
			_con_e = Connector.new(self)
			_con_n.inside = _con_e
			_con_e.inside = _con_n
		ModelEnums.Tiles.N_W:
			_con_n = Connector.new(self)
			_con_w = Connector.new(self)
			_con_n.inside = _con_w
			_con_w.inside = _con_n
		ModelEnums.Tiles.BOMB:
			pass
		_:
			breakpoint

func get_type() -> ModelEnums.Tiles:
	return _type
	
static func get_tile(connector : Connector) -> Array[TileModel]:
	var con := connector.outside
	var tiles :Array[TileModel]= [con.parent]
	
	while con.parent.get_type() != ModelEnums.Tiles.NORMAL and con.parent.get_type() != ModelEnums.Tiles.BOMB:
		con = con.inside.outside
		tiles.append(con.parent)
	
	return tiles

func get_direction(direction:ModelEnums.Directions) -> Array[TileModel]:
	match(direction):
		ModelEnums.Directions.NORTH:
			return get_north()
		ModelEnums.Directions.WEST:
			return get_west()
		ModelEnums.Directions.SOUTH:
			return get_south()
		ModelEnums.Directions.EAST:
			return get_east()
		_:
			breakpoint
	return []

func get_north() -> Array[TileModel]:
	return TileModel.get_tile(_con_n)

func get_west() -> Array[TileModel]:
	return TileModel.get_tile(_con_w)

func get_south() -> Array[TileModel]:
	return TileModel.get_tile(_con_s)

func get_east() -> Array[TileModel]:
	return TileModel.get_tile(_con_e)

func discover() -> void:
	_visible_until = Time.get_unix_time_from_system() + (_bad_visibility_time if _bad_visibility else 99999.9)
	_discovered = true
	_emit_tile_state_changed()

func is_discovered() -> bool:
	return _discovered

func bombify() -> void:
	set_type(ModelEnums.Tiles.BOMB)
	_is_bomb = true
	_is_bomb_close = false
	
	var tiles := get_tiles_around(1)
	
	for t in tiles:
		t._bomb_closeify()
	_emit_tile_state_changed()
		
func _bomb_closeify() -> void:
	if not (_is_beast or _is_bomb or _is_bomb_close):
		_is_bomb_close = true 
	_emit_tile_state_changed()

func beastify() -> void:
	_is_beast = true
	_emit_tile_state_changed()

	var tiles := get_tiles_around(2)
	for t in tiles:
		t._beast_closeify()
		
func _beast_closeify() -> void:
	if not (_is_beast or _is_bomb or _is_beast_close):
		_is_beast_close = true
	_emit_tile_state_changed()

func get_tiles_around(depth:int) -> Array[TileModel]:
	var queue :Array[Array]= [[self, depth]]
	var tiles :Array[TileModel]= []
	
	while !queue.is_empty():
		var processing :Array= queue.pop_front()
		var curr_tile :TileModel= processing[0]
		var curr_depth :int= processing[1]
		
		if !tiles.has(curr_tile):
			tiles.append(curr_tile)
			
			if curr_depth > 0:
				var new_d := curr_depth-1
				queue.append([curr_tile.get_north().pop_back(), new_d])
				queue.append([curr_tile.get_west().pop_back(), new_d])
				queue.append([curr_tile.get_south().pop_back(), new_d])
				queue.append([curr_tile.get_east().pop_back(), new_d])
	
	return tiles

func get_x() -> int:
	return _x

func get_y() -> int:
	return _y

func has_direction(d:ModelEnums.Directions) -> bool:
	match d:
		ModelEnums.Directions.NORTH:
			return _con_n != null
		ModelEnums.Directions.SOUTH:
			return _con_s != null
		ModelEnums.Directions.WEST:
			return _con_w != null
		ModelEnums.Directions.EAST:
			return _con_e != null
	return false

func set_has_player(tile_has_player:bool) -> void:
	_has_player = tile_has_player
	_emit_tile_state_changed()

func _emit_tile_state_changed() -> void:
	emit_signal("tile_state_changed")

func is_visible() -> bool:
	return _always_visible or _visible_until > Time.get_unix_time_from_system()

func is_beast() -> bool:
	return _is_beast

func is_bomb() -> bool:
	return _is_bomb

func has_player() -> bool:
	return _has_player

func is_beast_close() -> bool:
	return _is_beast_close

func is_bomb_close() -> bool:
	return _is_bomb_close

func is_always_visible() -> bool:
	return _always_visible

func get_visible_until() -> float:
	return _visible_until

func get_visible_time() -> float:
	var visible_time := _visible_until - Time.get_unix_time_from_system()
	return visible_time if visible_time > 0 else 0.0
