class_name Achievement

# unique id of the achievement
var _id : String
# current level of achievement
var _level : int
# the percentage completed for the next level.
# -1 if no more levels are available or percentage does not make sense
var _completion_percent : float
# level names
var _names : Array[String]
# level descriptions
var _descriptions : Array[String]

func _init(id:String, level:int, completion_percent:float, names:Array[String], descriptions:Array[String]) -> void:
	_id = id
	_level = level
	_completion_percent = completion_percent
	_descriptions = descriptions.duplicate()
	_names = names.duplicate()
	assert(level >= 0)
	assert(level <= len(names))
	assert(len(names) == len(descriptions))
	assert(completion_percent == -1 or (completion_percent >= 0 and completion_percent < 1))


func duplicate() -> Achievement:
	return Achievement.new(_id, _level, _completion_percent, _names.duplicate(), _descriptions.duplicate())

func get_max_level() -> int:
	return len(_names)

func get_name() -> String:
	return _names[_level] if _level != len(_names) else _names[_level-1]

func get_description() -> String:
	var description := _descriptions[_level] if _level != len(_descriptions) else _descriptions[_level-1]
	if _level > 0 and _level != len(_descriptions):
		description = _descriptions[_level-1] + "\nNext:\n" + description
	return description

func set_names(names:Array[String]) -> void:
	_names = names.duplicate()
	assert(len(_names) == len(_descriptions))

func set_descriptions(descriptions:Array[String]) -> void:
	_descriptions = descriptions.duplicate()
	assert(len(_descriptions) == len(_names))

func set_names_descriptions(names:Array[String], descriptions:Array[String]) -> void:
	_names = names
	_descriptions = descriptions
	assert(len(_names) == len(_descriptions))

func set_completion_percent(percent:float) -> void:
	_completion_percent = percent
	assert(_completion_percent == -1 or (_completion_percent >= 0 and _completion_percent < 1))

func set_level(level:int) -> void:
	_level = level
	assert(level >= 0)
	assert(level <= len(_names))
	
func get_id() -> String:
	return _id

func get_level() -> int:
	return _level

func get_names() -> Array[String]:
	return _names.duplicate()

func get_descriptions() -> Array[String]:
	return _descriptions.duplicate()

func get_completion_percent() -> float:
	return _completion_percent
