extends Sprite2D

@onready var M :Main= get_node("/root/Main")
@onready var LEFT :Button= get_node("left")
@onready var RIGHT :Button= get_node("right")

func _ready() -> void:
	LEFT.size.x = self.get_rect().size.x * 0.35
	RIGHT.size.y = self.get_rect().size.x * 0.35
	_set_texture(M.get_difficulty())

func _on_difficulty_changed(new_difficulty:ModelEnums.Difficulties) -> void:
	_set_texture(new_difficulty);

func _set_texture(diff:ModelEnums.Difficulties) -> void:
	match diff:
		ModelEnums.Difficulties.TRAINEE:
			self.texture = load("res://sprites/ui/trainee.png")
		ModelEnums.Difficulties.EASY:
			self.texture = load("res://sprites/ui/easy.png")
		ModelEnums.Difficulties.MEDIUM:
			self.texture = load("res://sprites/ui/medium.png")
		ModelEnums.Difficulties.HARD:
			self.texture = load("res://sprites/ui/hard.png")
