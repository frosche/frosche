class_name Enums

enum Scenes {
	LOAD,
	MAIN_MENU,
	GAME,
	STATS,
	OPTIONS,
	ACHIEVEMENTS,
	TUTORIAL
}

static func difficulties_to_readable(diff:ModelEnums.Difficulties) -> String:
	match diff:
		ModelEnums.Difficulties.TRAINEE:
			return "Trainee"
		ModelEnums.Difficulties.EASY:
			return "Easy"
		ModelEnums.Difficulties.MEDIUM:
			return "Medium"
		ModelEnums.Difficulties.HARD:
			return "Hard"
	breakpoint
	return "Trainee"
