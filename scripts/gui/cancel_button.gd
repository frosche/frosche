extends Button

func _init() -> void:
	self.visible = false

func _on_player_started_aim(_tile:TileModel, _direction:ModelEnums.Directions) -> void:
	self.visible = true

func _on_player_stopped_aim(_tile:TileModel, _direction:ModelEnums.Directions) -> void:
	self.visible = false

func _on_game_over_animation_finished() -> void:
	self.visible = false
