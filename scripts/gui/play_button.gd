extends Button

func press() -> void:
	emit_signal("pressed")
