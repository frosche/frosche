extends Sprite2D

@onready var M :Main= get_node("/root/Main")

func _on_screen_changed(_scene:Enums.Scenes):
	var extra_x := M.WINDOW_WIDTH - int(self.texture.get_size().x/2)
	self.position.x = (randi()%int(self.texture.get_size().x/2 - extra_x)) + extra_x
	var extra_y := M.WINDOW_HEIGHT - int(self.texture.get_size().y/2)
	self.position.y = (randi()%int(self.texture.get_size().y/2 - extra_y)) + extra_y
	
