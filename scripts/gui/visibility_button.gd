extends Button

@onready var M :Main= get_node("/root/Main")
@onready var TICK :Sprite2D= get_node("Tick") 

func _ready() -> void:
	TICK.visible = M.is_bad_visibility()

func _on_bad_visibility_changed(bad_visibility:bool) -> void:
	TICK.visible = bad_visibility
