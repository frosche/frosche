class_name Persist

var _data := {
	"difficulty" : ModelEnums.Difficulties.TRAINEE,
	"games" : [],
	"achievements" : [],
	"volume_music" : 1.0,
	"volume_ambient" : 1.0,
	"volume_effects" : 1.0,
	"tutorial_done" : false
}

const SAVE_GAME_PATH := "user://savegame.save"

func _init() -> void:
	_load()
	set_difficulty(get_difficulty()) # this is needed. don't ask

func _save() -> void:
	var save_game := FileAccess.open(SAVE_GAME_PATH, FileAccess.WRITE)
	var json_string := JSON.stringify(_data)
	save_game.store_line(json_string)

func _load() -> void:
	if not FileAccess.file_exists(SAVE_GAME_PATH):
		return
	var save_file := FileAccess.open(SAVE_GAME_PATH, FileAccess.READ)
	var json_string := save_file.get_line()

	var json := JSON.new()
	var parse_result := json.parse(json_string)
	if not parse_result == OK:
		print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
		breakpoint
		return

	var node_data :Dictionary= json.get_data()

	for i in node_data.keys():
		_data[i] = node_data[i]

func reset_games_played() -> void:
	_data["games"] = []
	_save()

func set_difficulty(diff:ModelEnums.Difficulties) -> void:
	_data["difficulty"] = diff
	_save()

func get_difficulty() -> ModelEnums.Difficulties:
	return _data["difficulty"]

func _on_game_over(won:bool, _bomb:bool, time:int, difficulty:ModelEnums.Difficulties, fog:bool, _tiles:Array[TileModel]) -> void:
	_data["games"].append({"difficulty": difficulty, "won":won, "time":time, "fog":fog})
	_save()

func get_games_stats() -> Dictionary:
	var stats := {
		"games_played_trainee" : 0,
		"games_won_trainee" : 0,
		"games_played_easy" : 0,
		"games_won_easy" : 0,
		"games_played_medium" : 0,
		"games_won_medium" : 0,
		"games_played_hard" : 0,
		"games_won_hard" : 0
	}
	for game in _data["games"]:
		match ModelEnums.to_difficulties(game["difficulty"]):
			ModelEnums.Difficulties.TRAINEE:
				stats["games_played_trainee"] += 1
				if game["won"]:
					stats["games_won_trainee"] += 1
			ModelEnums.Difficulties.EASY:
				stats["games_played_easy"] += 1
				if game["won"]:
					stats["games_won_easy"] += 1
			ModelEnums.Difficulties.MEDIUM:
				stats["games_played_medium"] += 1
				if game["won"]:
					stats["games_won_medium"] += 1
			ModelEnums.Difficulties.HARD:
				stats["games_played_hard"] += 1
				if game["won"]:
					stats["games_won_hard"] += 1
			_:
				breakpoint
	return stats

func get_games() -> Array:
	return _data["games"].duplicate(true)

func get_games_played(difficulty : ModelEnums.Difficulties) -> int:
	var num := 0
	for game in _data["games"]:
		if game["difficulty"] == difficulty:
			num += 1
	return num

func get_games_won(difficulty : ModelEnums.Difficulties) -> int:
	var num := 0
	for game in _data["games"]:
		if game["difficulty"] == difficulty and game["won"]:
			num += 1
	return num

func get_volume_music() -> float:
	return _data["volume_music"]

func get_volume_ambient() -> float:
	return _data["volume_ambient"]

func get_volume_effects() -> float:
	return _data["volume_effects"]

func set_volume_music(volume:float) -> void:
	_data["volume_music"] = volume
	_save()

func set_volume_ambient(volume:float) -> void:
	_data["volume_ambient"] = volume
	_save()

func set_volume_effects(volume:float) -> void:
	_data["volume_effects"] = volume
	_save()

func set_tutorial_done(done:bool) -> void:
	_data["tutorial_done"] = done
	_save()

func is_tutorial_done() -> bool:
	return _data["tutorial_done"]

func get_achievements() -> Array[Achievement]:
	var achievements :Array[Achievement]= []
	for a in _data["achievements"]:
		achievements.append(Achievement.new(a["id"], a["level"], a["completion_percent"], ["","","",""], ["","","",""]))
	return achievements

func add_achievement(achievement:Achievement) -> void:
	var found := false
	var dict := {"id":achievement.get_id(), "level":achievement.get_level(),
		"completion_percent":achievement.get_completion_percent()}
	for i in range(len(_data["achievements"])):
		if _data["achievements"][i]["id"] == achievement.get_id():
			_data["achievements"][i] = dict
			found = true
			break
	if not found:
		_data["achievements"].append(dict)
	_save()
