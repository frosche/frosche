extends Control

@onready var M :Main= get_node("/root/Main")

@onready var BOXES_ROOT := $ScrollContainer/VBoxContainer


func _ready() -> void:
	var achievements := M.get_achievements()
	for a in achievements:
		var box :AchievementBox= load("res://scenes/classes/AchievementBox.tscn").instantiate()
		box.set_title(a.get_name())
		box.set_description(a.get_description())
		box.set_stars(a.get_max_level(), a.get_level())
		if a.get_completion_percent() != -1:
			box.set_completion_percent(a.get_completion_percent())
		BOXES_ROOT.add_child(box)


func _notification(what) -> void:
	if what == NOTIFICATION_WM_GO_BACK_REQUEST:
		M.change_scene(Enums.Scenes.MAIN_MENU)


func _on_back_button_pressed() -> void:
	M.change_scene(Enums.Scenes.MAIN_MENU)
