extends CustomAudioPlayer

@onready var M :Main= get_node("/root/Main")

var _last_audio := (randi()%int(stream.get_length())) + randf();

func _ready() -> void:
	play(_last_audio)
	_base_volume = volume_db

func _on_finished() -> void:
	play()

func save_position() -> void:
	_last_audio = get_playback_position()
