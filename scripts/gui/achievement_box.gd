extends Control

class_name AchievementBox

@export var star_unfilled :Texture2D= load("res://sprites/star_unfilled.png")
@export var star_filled :Texture2D= load("res://sprites/star_filled.png")


func set_title(text:String) -> void:
	$title.text = text
	
	
func set_description(text:String) -> void:
	$description.text = text


func set_completion_percent(progress:float) -> void:
	$completion_bar.visible = true
	$completion_bar.value = progress


func set_stars(stars:int, filled:int) -> void:
	assert(stars <= 4)
	assert(filled >= 0)
	assert(filled <= stars)
	$stars_1.visible = false
	$stars_2.visible = false
	$stars_3.visible = false
	$stars_4.visible = false
	if stars == 1:
		$stars_1.visible = true
		$stars_1/star_1.texture = star_filled if filled > 0 else star_unfilled
	if stars == 2:
		$stars_2.visible = true
		$stars_2/star_1.texture = star_filled if filled > 0 else star_unfilled
		$stars_2/star_2.texture = star_filled if filled > 1 else star_unfilled
	if stars == 3:
		$stars_3.visible = true
		$stars_3/star_1.texture = star_filled if filled > 0 else star_unfilled
		$stars_3/star_2.texture = star_filled if filled > 1 else star_unfilled
		$stars_3/star_3.texture = star_filled if filled > 2 else star_unfilled
	if stars == 4:
		$stars_4.visible = true
		$stars_4/star_1.texture = star_filled if filled > 0 else star_unfilled
		$stars_4/star_2.texture = star_filled if filled > 1 else star_unfilled
		$stars_4/star_3.texture = star_filled if filled > 2 else star_unfilled
		$stars_4/star_4.texture = star_filled if filled > 3 else star_unfilled
