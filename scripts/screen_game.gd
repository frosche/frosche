extends Panel

class_name Game


@onready var MAP_ORIGIN :Node2D= get_node("MapOrigin")
@onready var PLAYER :Player= get_node("Player")
@onready var SPLASH_SPRITE : AnimatedSprite2D = get_node("Splash")
const SPLASH_SIZE_PX = 256
@onready var OK_BUTTON : Button = get_node("ok_button")
@onready var CANCEL_BUTTON : Button = get_node("cancel_button")
@onready var AUDIO_AMBIENT : AudioStreamPlayer = get_node("AudioAmbient")
@onready var AUDIO_JUMP : AudioStreamPlayer = get_node("AudioJump")
@onready var AUDIO_SPLASH : AudioStreamPlayer = get_node("AudioSplash")
@onready var AUDIO_NET_PULL : AudioStreamPlayer = get_node("AudioNetPull")
@onready var AUDIO_NET_HOLD : AudioStreamPlayer = get_node("AudioNetHold")
@onready var END_LABEL : Label = get_node("end_label")
@onready var TIMER_LABEL : Label = get_node("timer_label")

@onready var M :Main= get_node("/root/Main")

signal pre_game_start(game_model:GameModel)
signal game_over_animation_started()
signal game_over_animation_finished()

var _view_tiles :Array[Array]= []

var _game_model :GameModel= GameModel.new()
var _game_over_animation_ongoing := false

func _ready() -> void:
	$AudioAmbient.set_volume(M.get_ambient_volume())
	var effects_volume := M.get_effects_volume()
	$AudioJump.set_volume(effects_volume)
	$AudioSplash.set_volume(effects_volume)
	$AudioNetPull.set_volume(effects_volume)
	$AudioNetHold.set_volume(effects_volume)
	
	MAP_ORIGIN.position.x = M.WINDOW_WIDTH*0.025 + M.TILE_SIZE/2.0
	var splash_scale :=  M.TILE_SIZE*1.5 / float(SPLASH_SIZE_PX)
	SPLASH_SPRITE.scale = Vector2(splash_scale, splash_scale)
	PLAYER.scale_to(M.TILE_SIZE*0.8)
	
	var map_seed := M.get_seed()
	if map_seed == 0:
		randomize()
		map_seed = randi() % 10000
		print("seed: "+str(map_seed))
	$seed_label.text = "seed: "+str(map_seed)
	
	_game_model.set_seed(map_seed)
	_game_model.set_difficulty(M.get_difficulty())
	_game_model.set_bad_visibility(M.is_bad_visibility())
	_game_model.connect("game_started", Callable(self, "_on_game_started"))
	_game_model.connect("game_over", Callable(self, "_on_game_over"))
	_game_model.connect("game_over", Callable(END_LABEL, "_on_game_over"))
	_game_model.connect("game_time_passed", Callable(TIMER_LABEL, "_on_game_time_passed"))
	_emit_pre_game_start(_game_model)
	_game_model.start_game()

func _on_game_started(tiles:Array[TileModel], player:PlayerModel) -> void:
	player.connect("started_aim", Callable(self, "_on_player_started_aim"))
	player.connect("stopped_aim", Callable(self, "_on_player_stopped_aim"))
	player.connect("started_aim", Callable(CANCEL_BUTTON, "_on_player_started_aim"))
	player.connect("stopped_aim", Callable(CANCEL_BUTTON, "_on_player_stopped_aim"))
	player.connect("stepped", Callable(self, "_on_player_stepped"))
	PLAYER.set_player_model(player)
	
	_view_tiles = []
	for y in _game_model.get_height():
		var row := []
		for x in _game_model.get_width():
			row.append(null)
		_view_tiles.append(row)
	for model_tile in tiles:
		var tile := M.TILE_SCENE.instantiate()
		MAP_ORIGIN.add_child(tile)
		tile.set_model_tile(model_tile)
		_view_tiles[model_tile.get_y()][model_tile.get_x()] = tile

		if model_tile.has_player():
			PLAYER.position = tile.global_position

func _process(delta:float) -> void:
	_game_model.tick(delta)

func get_tile_view(model_tile:TileModel) -> Tile:
	return _view_tiles[model_tile.get_y()][model_tile.get_x()]

func _physics_process(_delta:float) -> void:
	if _game_model.is_game_ongoing():
		if Input.is_action_just_pressed("shoot"):
			get_node("shoot_button").press()
		if Input.is_action_just_pressed("go_north"):
			get_node("north_button").press()
		if Input.is_action_just_pressed("go_west"):
			get_node("west_button").press()
		if Input.is_action_just_pressed("go_south"):
			get_node("south_button").press()
		if Input.is_action_just_pressed("go_east"):
			get_node("east_button").press()
	else:
		if not _game_over_animation_ongoing and Input.is_action_just_pressed("shoot"):
			OK_BUTTON.press()

func _on_game_over(_won:bool, bomb:bool, _time:int, _difficulty:ModelEnums.Difficulties, _fog:bool, _tiles:Array[TileModel]) -> void:
	if bomb:
		await PLAYER.step_animation_finished
	PLAYER.visible = false
	var end_position := PLAYER.global_position
	_emit_game_over_animation_started()
	_game_over_animation_ongoing = true
	if bomb:
		SPLASH_SPRITE.global_position = end_position
		SPLASH_SPRITE.visible = true
		AUDIO_SPLASH.play()
		SPLASH_SPRITE.play()
	else:
		_game_over_animation_finished()
	
func _on_ok_button_pressed() -> void:
	AUDIO_AMBIENT.save_position()
	M.change_scene(Enums.Scenes.MAIN_MENU)

func _on_player_shoot() -> void:
	AUDIO_NET_HOLD.stop()
	AUDIO_NET_PULL.play()

func _on_splash_animation_looped() -> void:
	SPLASH_SPRITE.stop()
	SPLASH_SPRITE.visible = false
	_game_over_animation_finished()

func _game_over_animation_finished() -> void:
	_game_over_animation_ongoing = false
	for tile in MAP_ORIGIN.get_children():
		tile.set_always_visible(true)
	_emit_game_over_animation_finished()

func _emit_game_over_animation_started() -> void:
	emit_signal("game_over_animation_started")

func _emit_game_over_animation_finished() -> void:
	emit_signal("game_over_animation_finished")

func _emit_pre_game_start(game_model:GameModel) -> void:
	emit_signal("pre_game_start", game_model)

func _on_player_started_aim(_tile:TileModel, _direction:ModelEnums.Directions) -> void:
	AUDIO_NET_PULL.play()

func _on_player_stopped_aim(_tile:TileModel, _direction:ModelEnums.Directions) -> void:
	AUDIO_NET_HOLD.stop()

func _on_audio_net_pull_finished() -> void:
	if _game_model.get_player().is_aiming():
		AUDIO_NET_HOLD.play()

func _on_audio_net_hold_finished() -> void:
	if _game_model.get_player().is_aiming():
		AUDIO_NET_HOLD.play()

func _on_player_stepped(path:Array[TileModel]) -> void:
	var tile := get_tile_view(path[-1])
	PLAYER.start_move_animation(tile.global_position)
	AUDIO_JUMP.stream = M.get_jump_sound()
	AUDIO_JUMP.play()

func get_game_model() -> GameModel:
	return _game_model

func _on_north_button_pressed() -> void:
	PLAYER.queue_direction(ModelEnums.Directions.NORTH)

func _on_west_button_pressed() -> void:
	PLAYER.queue_direction(ModelEnums.Directions.WEST)

func _on_south_button_pressed() -> void:
	PLAYER.queue_direction(ModelEnums.Directions.SOUTH)

func _on_east_button_pressed() -> void:
	PLAYER.queue_direction(ModelEnums.Directions.EAST)

func _on_shoot_button_pressed() -> void:
	PLAYER.shoot()

func _on_cancel_button_pressed() -> void:
	PLAYER.stop_aim()
