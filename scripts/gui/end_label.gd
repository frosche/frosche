extends Label

func _on_game_over(won:bool, _bomb:bool, _time:int, _difficulty:ModelEnums.Difficulties, _fog:bool, _tiles:Array[TileModel]) -> void:
	if won:
		self.text = "You caught the fly!"
	else:
		self.text = "You lost."

func _on_screen_game_game_over_animation_finished() -> void:
	self.visible = true
