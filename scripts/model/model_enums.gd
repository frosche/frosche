class_name ModelEnums

enum Directions {
	NORTH,
	WEST,
	SOUTH,
	EAST
}

enum Tiles {
	NONE,
	NORMAL,
	S_N,
	W_E,
	S_E,
	S_W,
	N_E,
	N_W,
	BOMB
}

enum Difficulties {
	TRAINEE,
	EASY,
	MEDIUM,
	HARD
}

static func to_difficulties(diff) -> Difficulties:
	match diff:
		0, 0.0, Difficulties.TRAINEE:
			return Difficulties.TRAINEE
		1, 1.0, Difficulties.EASY:
			return Difficulties.EASY
		2, 2.0, Difficulties.MEDIUM:
			return Difficulties.MEDIUM
		3, 3.0, Difficulties.HARD:
			return Difficulties.HARD
	breakpoint
	return Difficulties.TRAINEE

static func opposite_direction(direction:Directions) -> Directions:
	match direction:
		Directions.NORTH:
			return Directions.SOUTH
		Directions.SOUTH:
			return Directions.NORTH
		Directions.EAST:
			return Directions.WEST
		Directions.WEST:
			return Directions.EAST
	breakpoint
	return Directions.NORTH
