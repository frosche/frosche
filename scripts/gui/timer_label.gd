extends Label

func _on_game_time_passed(time:float, _delta:float) -> void:
	self.text = str(int(time/60.0))+":"+("0" if (int(time)%60)<10 else "")+str(int(time)%60)
