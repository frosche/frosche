class_name PlayerModel

signal started_aim(tile:TileModel, direction:ModelEnums.Directions)
signal stopped_aim(tile:TileModel, direction:ModelEnums.Directions)
signal changed_aim_direction(tile:TileModel, direction:ModelEnums.Directions)
signal shot(from:TileModel, to:TileModel, direction:ModelEnums.Directions)
signal stepped(path:Array[TileModel])

var _tile :TileModel= null

var _alive := true
var _aiming := false
var _aim_direction := ModelEnums.Directions.NORTH

func set_tile(tile:TileModel) -> void:
	assert(tile != null)
	_tile = tile

func tick(_delta:float) -> void:
	if _alive:
		_tile.discover()

func move(direction:ModelEnums.Directions) -> void:
	var route := _tile.get_direction(direction)
	route.push_front(_tile)
	for tile in route:
		tile.discover()
	_tile.set_has_player(false)
	_tile = route[-1]
	_tile.set_has_player(true)
	_emit_stepped(route)

func start_aim() -> void:
	_aiming = true
	set_aim_direction(ModelEnums.Directions.NORTH)
	_emit_started_aim()

func stop_aim() -> void:
	_aiming = false
	_emit_stopped_aim()

func set_aim_direction(direction:ModelEnums.Directions) -> void:
	_aim_direction = direction
	_emit_changed_aim_direction()

func shoot() -> void:
	stop_aim()
	var shoot_tile : TileModel
	match(_aim_direction):
		ModelEnums.Directions.NORTH:
			shoot_tile = _tile.get_north().pop_back()
		ModelEnums.Directions.WEST:
			shoot_tile = _tile.get_west().pop_back()
		ModelEnums.Directions.SOUTH:
			shoot_tile = _tile.get_south().pop_back()
		ModelEnums.Directions.EAST:
			shoot_tile = _tile.get_east().pop_back()
		_:
			breakpoint
	assert(shoot_tile != null)
	_emit_shot(_tile, shoot_tile, _aim_direction)

func _emit_started_aim() -> void:
	emit_signal("started_aim", _tile, _aim_direction)

func _emit_stopped_aim() -> void:
	emit_signal("stopped_aim", _tile, _aim_direction)

func _emit_stepped(route:Array[TileModel]) -> void:
	emit_signal("stepped", route)

func _emit_shot(from:TileModel, to:TileModel, direction:ModelEnums.Directions) -> void:
	emit_signal("shot", from, to, direction)

func _emit_changed_aim_direction() -> void:
	emit_signal("changed_aim_direction", _tile, _aim_direction)

func is_alive() -> bool:
	return _alive

func is_aiming() -> bool:
	return _aiming

func set_alive(alive:bool) -> void:
	_alive = alive

func get_tile() -> TileModel:
	return _tile

func get_aim_direction() -> ModelEnums.Directions:
	return _aim_direction
