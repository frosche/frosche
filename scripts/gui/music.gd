extends CustomAudioPlayer


@onready var M :Main= get_node("/root/Main")


func _ready() -> void:
	_base_volume = volume_db
	play()


func _on_finished() -> void:
	play()


func _on_music_volume_changed(volume) -> void:
	set_volume(volume)
