# Frog hat
![screenshot of the game](docs/screenshot.png "Screenshot of the Game")

# How to get
The game may be downloaded from [itch.io](https://khlte.itch.io/phrogg) or from the gitlab CI/CD pipeline directly.

It is not yet public on the play store.

# How to build
The game was made using [Godot 4.1](https://godotengine.org) and can be imported and compiled like any other game. Primary focus is on the Android build, however, web, linux and windows builds should work as well. Built files are available for the app from the [CI pipeline](https://gitlab.com/frosche/frosche/-/pipelines) as well as published on [itch.io](https://khlte.itch.io/phrogg)
