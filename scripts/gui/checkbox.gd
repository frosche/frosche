extends CheckButton

@export var label_text := "text"


func _ready() -> void:
	set_label(label_text)


func set_label(string:String) -> void:
	$label.text = string
