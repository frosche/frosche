extends Control

@onready var M :Main= get_node("/root/Main")


func _ready() -> void:
	refresh_stats()


func _notification(what):
	if what == NOTIFICATION_WM_GO_BACK_REQUEST:
		M.change_scene(Enums.Scenes.MAIN_MENU)


func refresh_stats() -> void:
	var stats := M.get_games_stats()
	
	$trainee_stat.text = str(stats["games_won_trainee"])+" / "+str(stats["games_played_trainee"])
	$easy_stat.text = str(stats["games_won_easy"])+" / "+str(stats["games_played_easy"])
	$medium_stat.text = str(stats["games_won_medium"])+" / "+str(stats["games_played_medium"])
	$hard_stat.text = str(stats["games_won_hard"])+" / "+str(stats["games_played_hard"])


func _on_back_button_pressed() -> void:
	M.change_scene(Enums.Scenes.MAIN_MENU)


func _on_clear_button_pressed() -> void:
	M.reset_stats()
	refresh_stats()
