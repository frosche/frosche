extends Control

@onready var M :Main= get_node("/root/Main")

@onready var PLAY_BUTTON :Button= get_node("play_button")


func _physics_process(_delta:float) -> void:
	if Input.is_action_just_pressed("shoot"):
		PLAY_BUTTON.press()
		return

func _ready() -> void:
	var ret := get_node("play_button").connect("pressed", Callable(M, "_on_play_pressed"))
	assert(!ret)
	ret = get_node("difficulty_button/left").connect("pressed", Callable(M, "_on_difficulty_decrease"))
	assert(!ret)
	ret = get_node("difficulty_button/right").connect("pressed", Callable(M, "_on_difficulty_increase"))
	assert(!ret)
	ret = M.connect("difficulty_changed", Callable(get_node("difficulty_button"), "_on_difficulty_changed"))
	assert(!ret)
	ret = get_node("visibility_button").connect("pressed", Callable(M, "_on_visibility_pressed"))
	assert(!ret)
	ret = get_node("seed_spin").connect("value_changed", Callable(M, "_on_seed_changed"))
	assert(!ret)
	ret = M.connect("bad_visibility_changed", Callable(self, "_on_bad_visibility_changed"))
	assert(!ret)
	ret = M.connect("seed_changed", Callable(self, "_on_seed_changed"))
	assert(!ret)
	
	$visibility_button.button_pressed = M.is_bad_visibility()
	$seed_spin.value = M.get_seed()
	
	for i in range(1, 11):
		get_node("last_games_label/last_game"+str(i)).text = ""

	var games := M.get_games() 
	var num := 10 if len(games) >= 10 else len(games)
	for i in range(1, num+1):
		var game :Dictionary= games[-i]
		var label :Label= get_node("last_games_label/last_game"+str(i))
		label.text = Enums.difficulties_to_readable(game["difficulty"]) \
		+ " | " \
		+ ("0" if int(game["time"]/60.0)==0 else str(int(game["time"]/60.0))) \
		+ ":" \
		+ ("0" if int(game["time"])%60 < 10 else "") \
		+ str(int(game["time"])%60)
		label.set("theme_override_colors/font_color", (Color.GREEN if game["won"] else Color.RED))


func _on_seed_changed(value:int) -> void:
	$seed_spin.value = value


func _on_stats_button_pressed() -> void:
	M.change_scene(Enums.Scenes.STATS)


func _on_achievements_button_pressed() -> void:
	M.change_scene(Enums.Scenes.ACHIEVEMENTS)


func _on_options_button_pressed() -> void:
	M.change_scene(Enums.Scenes.OPTIONS)

func _on_bad_visibility_changed(visibility:bool) -> void:
	$visibility_button.button_pressed = visibility


func add_achievements(achievements:Array[Dictionary]) -> void:
	if len(achievements) == 0:
		$achievements_star.visible = false
		$achievements_label.visible = false
		return
	
	$achievements_star/AnimationPlayer.play("star_spin")
	$achievements_star.visible = true
	$achievements_label.visible = true
	
	for i in range(0, $achievements_label.get_child_count()):
		$achievements_label.get_child(i).queue_free()
	
	if len(achievements) > 1:
		$achievements_label.text = "Achievements unlocked:"
	else:
		$achievements_label.text = "Achievement unlocked:"
	
	const separation := 60
	var current_separation := separation
	for achievement in achievements:
		var label := Label.new()
		label.text = achievement["name"]
		if achievement["level"] > 1:
			label.text += " "+str(achievement["level"])
		label.position.y += current_separation
		$achievements_label.add_child(label)
		current_separation += separation
	
	
func _on_star_spin_animation_finished(_anim_name) -> void:
	$achievements_star/AnimationPlayer.play("star_spin")
