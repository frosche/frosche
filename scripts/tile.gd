extends Node2D

class_name Tile

@onready var M :Main= get_node("/root/Main")

@onready var _sprite :Sprite2D= $TileSprite
@onready var _beast_close_sprite :Sprite2D= $BeastClose
@onready var _beast_close_frog_sprite :Sprite2D= $BeastCloseFrog
@onready var _bomb_close_sprite :Sprite2D= $BombClose
@onready var _bomb_close_frog_sprite :Sprite2D= $BombCloseFrog
@onready var _beast :Sprite2D= $Beast

@onready var _random_bomb_texture := M.get_texture(ModelEnums.Tiles.BOMB)

var _tile : TileModel
var _always_visible := false

func _ready() -> void:
	_beast_close_sprite.visible = false
	_bomb_close_sprite.visible = false
	_bomb_close_frog_sprite.visible = false
	_beast_close_frog_sprite.visible = false
	_beast.visible = false

func _process(_delta:float) -> void:
	if _always_visible or _tile.is_always_visible() or M.is_debug(): 
		self.modulate.a = 1
	else:
		var visible_for := _tile.get_visible_time()
		if visible_for == 0.0:
			self.modulate.a = 0
		elif visible_for <= M.FADING_TIME:
			self.modulate.a = visible_for / M.FADING_TIME
		else:
			self.modulate.a = 1

func set_model_tile(model_tile:TileModel) -> void:
	_tile = model_tile
	set_texture(M.get_texture(_tile.get_type()))
	size_to(M.TILE_SIZE)
	position = Vector2(_tile.get_x()*M.TILE_SIZE, _tile.get_y()*M.TILE_SIZE)
	_tile.connect("tile_state_changed", Callable(self, "_on_tile_state_changed"))
	self.modulate.a = 0
	_on_tile_state_changed()

func size_to(size : int) -> void:
	assert(_sprite.texture != null, "texture was null when size_to was called")
	var new_scale := float(size) / _sprite.texture.get_size().x
	scale.x = new_scale
	scale.y = new_scale

func set_texture(texture : Texture2D) -> void:
	_sprite.texture = texture

func _on_tile_state_changed() -> void:
	_beast.visible = _tile.is_beast()
	if _tile.is_bomb():
		_sprite.texture = _random_bomb_texture
	else:
		if _tile.has_player():
			_beast_close_sprite.visible = false
			_bomb_close_sprite.visible = false
			_beast_close_frog_sprite.visible = _tile.is_beast_close()
			_bomb_close_frog_sprite.visible = _tile.is_bomb_close()
		else:
			_beast_close_sprite.visible = _tile.is_beast_close()
			_bomb_close_sprite.visible = _tile.is_bomb_close()
			_beast_close_frog_sprite.visible = false
			_bomb_close_frog_sprite.visible = false
		_bomb_close_frog_sprite.position.x = 75 if _tile.is_beast_close() else 0
		_beast_close_frog_sprite.position.x = -75 if _tile.is_bomb_close() else 0

func set_always_visible(always_visible:bool) -> void:
	_always_visible = always_visible
