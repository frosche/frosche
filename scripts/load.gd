extends Control

signal loading_finished()
signal loaded_stage(stage:int, num_stages:int, next_load_string:String)

@onready var M :Main= get_node("/root/Main")

const NUM_STAGES := 5
var _current_stage := 0

func _process(_delta:float) -> void:
	load_stage()

func load_stage() -> void:
	var next_loading_string := ""
	if _current_stage == 1:
		# jump sounds
		for i in range(1, M.NUM_JUMP_SOUNDS+1):
			M._jump_sounds.append(load("res://sounds/jump/jump_"+str(i)+".ogg"))
		next_loading_string = "Growing lilypads.."
	elif _current_stage == 2:
		# lily mask
		var lilymask_stream := load("res://sprites/beasthunt_lilymask-01.png")
		var lilymask_image :Image= lilymask_stream.get_image()
		assert(lilymask_image.get_size().x > M.TILE_SIZE)
		assert(lilymask_image.get_size().y > M.TILE_SIZE)
		
		# normal lily pads
		for i in range(M.NUM_TILE_TEXTURES):
			var path := "res://sprites/tiles/lilypad_"
			var texture_stream := load(path+str(i)+".png")
			var texture_image :Image= texture_stream.get_image()
			
			var mask_stream := load(path+"mask_"+str(i)+".png")
			var mask_image :Image= mask_stream.get_image()
			
			var image := Image.create(texture_image.get_width(), texture_image.get_height(), false, Image.FORMAT_RGBA8)
			
			var offset_x = randi()%(int(lilymask_image.get_size().x)-image.get_width())
			var offset_y = randi()%(int(lilymask_image.get_size().y)-image.get_height())
			
			for y in image.get_height():
				for x in image.get_width():
					var color := Color()
					color += lilymask_image.get_pixel(offset_x+x, offset_y+y)
					color *= Color(1,1,1,1)-mask_image.get_pixel(x,y)
					if texture_image.get_pixel(x, y).a > 0:
						color = texture_image.get_pixel(x, y)
					image.set_pixel(x, y, color)
			
			var image_texture := ImageTexture.create_from_image(image)
			M._tile_textures.append(image_texture)
		assert(len(M._tile_textures) == M.NUM_TILE_TEXTURES)
		next_loading_string = "Watering lilypads..."
	elif _current_stage == 3:
		# lily mask
		var lilymask_stream := load("res://sprites/beasthunt_lilymask-01.png")
		var lilymask_image :Image= lilymask_stream.get_image()
		assert(lilymask_image.get_size().x > M.TILE_SIZE)
		assert(lilymask_image.get_size().y > M.TILE_SIZE)
		
		# broken lily pads
		for i in range(M.NUM_TILE_BROKEN_TEXTURES):
			var path := "res://sprites/tiles_broken/lilypad_broken_"
			var texture_stream := load(path+str(i)+".png")
			var texture_image :Image= texture_stream.get_image()
			
			var mask_stream := load(path+"mask_"+str(i)+".png")
			var mask_image :Image= mask_stream.get_image()
			
			var image := Image.create(texture_image.get_width(), texture_image.get_height(), false, Image.FORMAT_RGBA8)
			
			var offset_x := randi()%(int(lilymask_image.get_size().x)-image.get_width())
			var offset_y := randi()%(int(lilymask_image.get_size().y)-image.get_height())
			
			for y in image.get_height():
				for x in image.get_width():
					var color := Color()
					color += lilymask_image.get_pixel(offset_x+x, offset_y+y)
					color *= Color(1,1,1,1)-mask_image.get_pixel(x,y)
					var tex_col := texture_image.get_pixel(x, y)
					if tex_col.a > 0:
						if tex_col.a > 0.4:
							color = tex_col
						else:
							color.blend(tex_col)
					image.set_pixel(x, y, color)
			
			var image_texture := ImageTexture.create_from_image(image)
			M._tile_broken_textures.append(image_texture)
		assert(len(M._tile_broken_textures) == M.NUM_TILE_BROKEN_TEXTURES)
		next_loading_string = "Catching frog..."
	elif _current_stage == 4:
		# player
		var player_path := "res://sprites/phrogg.png"
		var player_stream := load(player_path)
		var player_image :Image= player_stream.get_image()
		player_image.resize(int(M.TILE_SIZE*0.8), int(M.TILE_SIZE*0.8))
		M.PLAYER_TEXTURE = ImageTexture.create_from_image(player_image)
		next_loading_string = "Finding lake..."
	elif _current_stage == 5:
		M._main_menu_scene = load("res://scenes/ScreenMainMenu.tscn")
		M._stats_scene = load("res://scenes/ScreenStats.tscn")
		M._game_scene = load("res://scenes/ScreenGame.tscn")
		M._options_scene = load("res://scenes/ScreenOptions.tscn")
		M._achievements_scene = load("res://scenes/ScreenAchievements.tscn")
		M._tutorial_scene = load("res://scenes/ScreenTutorial.tscn")

	if _current_stage <= NUM_STAGES:
		emit_signal("loaded_stage", _current_stage, NUM_STAGES, next_loading_string)
	else:
		emit_signal("loading_finished")
	_current_stage += 1
