class_name AchievementManager

signal achievement_unlocked(id:String, name:String, level:int)

var _persist : Persist
var _games : Array
var _games_won : Array[int]
var _achievements : Dictionary

# variables to keep track of the current game
var _jumps_this_game := 0
var _aimed_at_fly := false
var _aim_directions :Array[ModelEnums.Directions]= []

func _init(persist:Persist) -> void:
	_persist = persist
	_games = _persist.get_games()
	_games_won = [0,0,0,0]
	for game in _games:
		if game["won"]:
			_games_won[game["difficulty"]] += 1
	_init_achievements()


func get_achievements() -> Array[Achievement]:
	var to_return :Array[Achievement]= []
	for a in _achievements:
		to_return.append(_achievements[a].duplicate())
	return to_return


func save(id:="") -> void:
	if id != "":
		assert(_achievements.has(id))
		_persist.add_achievement(_achievements[id])
	else:
		for key in _achievements:
			_persist.add_achievement(_achievements[key])


func clear_stats() -> void:
	_games.clear()
	_games_won = [0,0,0,0]


func _total_games_won() -> int:
	var s := 0
	for n in _games_won:
		s += n
	return s


func _on_achievement_unlocked(achievement:Achievement) -> void:
	print("Achievement unlocked: "+achievement.get_id())
	save(achievement.get_id())
	emit_signal("achievement_unlocked", achievement.get_id(), achievement.get_name(), achievement.get_level())


func _add_achievement(id:String, names:Array[String], descriptions:Array[String], percentages:=false) -> void:
	if not _achievements.has(id):
		_achievements[id] = Achievement.new(id, 0, 0.0 if percentages else -1.0, names, descriptions)
	else:
		_achievements[id].set_names_descriptions(names, descriptions)


func _init_achievements() -> void:
	_achievements.clear()
	var existing := _persist.get_achievements()
	for a in existing:
		_achievements[a.get_id()] = a
	_add_achievement("BEAT_DIFFICULTY", ["Green Frog", "Trained Frog", "Confident Frog", "Ultimate Frog"], ["Beat the game on Trainee difficulty", "Beat the game on Easy difficulty", "Beat the game on Medium difficulty", "Beat the game on Hard difficulty"])
	_add_achievement("STRATEGIST", ["Strategist Rookie", "Strategist", "Strategist Master"], ["Win in under 25 jumps on Medium difficulty or above", "Win in under 15 jumps on Medium difficulty or above", "Win in under 8 jumps on Medium difficulty or above"])
	_add_achievement("SPEEDRUNNER", ["Speedrunner Rookie", "Speedrunner", "Speedrunner Master"], ["Win in under 30s on Medium difficulty or above", "Win in under 20s on Medium difficulty or above", "Win in under 10s on Medium difficulty or above"])
	_add_achievement("EXPLORER", ["Explorer"], ["Discover every lilypad that is not broken or the fly and win the game on Medium difficulty or higher"])
	_add_achievement("RISK_IT", ["Risk it"], ["Survive a jump from a danger indicator to an undiscovered tile"])
	_add_achievement("POOL_PARTY", ["Pool Party"], ["Fall in the pond"])
	_add_achievement("AROUND_THE_WORLD", ["Around the World"], ["Loop around to the same lilypad with a jump"])
	_add_achievement("DIDNT_SEE_YOU_THERE", ["Didn't see you there"], ["Bump into the fly"])
	_add_achievement("NEAR_MISS", ["Near miss"], ["Strike with your net on a lilypad next to the fly"])
	_add_achievement("LONG_JUMP", ["Long Jump", "Longer Jump", "Longest Jump"], ["Jump over 2 tiles at once", "Jump over 5 tiles at once", "Jump over 8 tiles at once"])
	_add_achievement("GREAT_THINKER", ["Great Thinker"], ["Play a game for over 10 minutes"])
	_add_achievement("CONSISTENCY_IS_KEY", ["Consistency is Key."], ["Win 10 games on Medium or higher difficulty in a row"], true)
	_add_achievement("CONSISTENCY_IS_KING", ["Consistency is King!"], ["Have your history consist entirely of wins, with at least 10 wins on Medium or higher difficulty"], true)
	_add_achievement("EXPERT_PERFORMANCE", ["Expert Performance"], ["Win in under 10 seconds on Hard difficulty"])
	_add_achievement("SEASONED_HUNTER", ["Seasoned Hunter"], ["Catch the fly 100 times"], true)
	_add_achievement("VETERAN", ["Veteran"], ["Complete 500 games"], true)
	_add_achievement("THINK_TWICE", ["Think Twice"], ["Cancel the catch"])
	_add_achievement("SMART_MOVE", ["Smart Move"], ["Cancel the catch on a lilypad near but not adjacent to the fly"])
	_add_achievement("SO_CLOSE", ["So Close"], ["Aim at the fly and then lose the game"])
	_add_achievement("MLG", ["MLG"], ["360 no scope the fly."])
	_add_achievement("SNIPER", ["Sniper", "Steady Shot", "Finnish Shot"], ["Catch the fly from 2 tiles away", "Catch the fly from 5 tiles away", "Catch the fly from 8 tiles away"])
	_add_achievement("BLINDFOLDED", ["Blindfolded", "Which way was it again..?", "Who needs eyes?"], ["Catch the fly on Easy difficulty with fog enabled", "Catch the fly on Medium difficulty with fog enabled", "Catch the fly on Hard difficulty with fog enabled"])
	save()


func _on_game_started(_tiles:Array[TileModel], _player:PlayerModel) -> void:
	_jumps_this_game = 0
	_aimed_at_fly = false
	_aim_directions = []


func _on_game_over(won:bool, _bomb:bool, time:int, difficulty:ModelEnums.Difficulties, fog:bool, tiles:Array[TileModel]) -> void:
	_games.append({"difficulty": difficulty, "won":won, "time":time, "fog":fog})
	
	var veteran :Achievement= _achievements["VETERAN"]
	if veteran.get_level() < 1:
		if len(_games) >= 500:
			veteran.set_level(1)
			_on_achievement_unlocked(veteran)
			veteran.set_completion_percent(-1)
		else:
			veteran.set_completion_percent(len(_games)/500.0)
	
	if time > 10 * 60:
		var great_thinker :Achievement= _achievements["GREAT_THINKER"]
		if great_thinker.get_level() < 1:
			great_thinker.set_level(1)
			_on_achievement_unlocked(great_thinker)
	
	var consistency_key :Achievement= _achievements["CONSISTENCY_IS_KEY"]
	var consistency_king :Achievement= _achievements["CONSISTENCY_IS_KING"]
	if won:
		_games_won[difficulty] += 1
		
		var win_streak_medium_plus := 0
		var index := 1
		while index <= len(_games) and _games[-index]["won"] and _games[-index]["difficulty"] >= ModelEnums.Difficulties.MEDIUM:
			win_streak_medium_plus += 1
			index += 1
		if consistency_key.get_level() < 1:
			if win_streak_medium_plus >= 10:
				consistency_key.set_level(1)
				_on_achievement_unlocked(consistency_key)
				consistency_key.set_completion_percent(-1)
			else:
				if win_streak_medium_plus == 0:
					consistency_key.set_completion_percent(0)
				else:
					consistency_key.set_completion_percent(win_streak_medium_plus/10.0)
		
		if consistency_king.get_level() < 1:
			if _total_games_won() == len(_games):
				if len(_games) >= 10:
					consistency_king.set_level(1)
					_on_achievement_unlocked(consistency_king)
				else:
					if len(_games) == 0:
						consistency_king.set_completion_percent(0)
					else:
						consistency_king.set_completion_percent(len(_games)/10.0)
			else:
				consistency_king.set_completion_percent(0)
		
		var seasoned :Achievement= _achievements["SEASONED_HUNTER"]
		if seasoned.get_level() < 1:
			if _total_games_won() > 100:
				seasoned.set_level(1)
				_on_achievement_unlocked(seasoned)
				seasoned.set_completion_percent(-1)
			else:
				seasoned.set_completion_percent(_total_games_won()/100.0)
		
		var won_level := int(difficulty) + 1
		var beat_difficulty :Achievement= _achievements["BEAT_DIFFICULTY"]
		if beat_difficulty.get_level() < won_level:
			beat_difficulty.set_level(won_level)
			_on_achievement_unlocked(beat_difficulty)
		if fog:
			var blindfolded_won_level = won_level-1
			var blindfolded :Achievement= _achievements["BLINDFOLDED"]
			if blindfolded.get_level() < blindfolded_won_level:
				blindfolded.set_level(blindfolded_won_level)
				_on_achievement_unlocked(blindfolded)
		
		if difficulty >= ModelEnums.Difficulties.MEDIUM:
			var speedrun_level := 0
			var speedrun :Achievement= _achievements["SPEEDRUNNER"]
			if time < 10:
				speedrun_level = 3
			elif time < 20:
				speedrun_level = 2
			elif time < 30:
				speedrun_level = 1
			if speedrun_level > speedrun.get_level():
				speedrun.set_level(speedrun_level)
				_on_achievement_unlocked(speedrun)
			
			if speedrun_level == 3 and difficulty == ModelEnums.Difficulties.HARD:
				var expert :Achievement= _achievements["EXPERT_PERFORMANCE"]
				if expert.get_level() < 1:
					expert.set_level(1)
					_on_achievement_unlocked(expert)
			
			var all_tiles_discovered := true
			for _t in tiles:
				var tile :TileModel= _t
				if tile.get_type() == ModelEnums.Tiles.NORMAL and not tile.is_discovered() and not tile.is_beast():
					all_tiles_discovered = false
					break
			if all_tiles_discovered:
				var explorer :Achievement= _achievements["EXPLORER"]
				if explorer.get_level() < 1:
					explorer.set_level(1)
					_on_achievement_unlocked(explorer)
			
			var strategist_level := 0
			var strategist :Achievement= _achievements["STRATEGIST"]
			if _jumps_this_game < 8:
				strategist_level = 3
			elif _jumps_this_game < 15:
				strategist_level = 2
			elif _jumps_this_game < 25:
				strategist_level = 1
			if strategist_level > strategist.get_level():
				strategist.set_level(strategist_level)
				_on_achievement_unlocked(strategist)
	else: # not won
		if _aimed_at_fly:
			var so_close :Achievement= _achievements["SO_CLOSE"]
			if so_close.get_level() < 1:
				so_close.set_level(1)
				_on_achievement_unlocked(so_close)
		if consistency_key.get_level() < 1:
			consistency_key.set_completion_percent(0)
		if consistency_king.get_level() < 1:
			consistency_king.set_completion_percent(0)


func _on_player_stepped(path:Array[TileModel]) -> void:
	_jumps_this_game += 1
	var start :TileModel= path[0]
	var end :TileModel= path[-1]
	if end.get_type() == ModelEnums.Tiles.BOMB:
		var pool_party :Achievement= _achievements["POOL_PARTY"]
		if pool_party.get_level() < 1:
			pool_party.set_level(1)
			_on_achievement_unlocked(pool_party)
	
	if end.is_beast():
		var didnt_see_you :Achievement= _achievements["DIDNT_SEE_YOU_THERE"]
		if didnt_see_you.get_level() < 1:
			didnt_see_you.set_level(1)
			_on_achievement_unlocked(didnt_see_you)
	
	if end.get_x() == start.get_x() and end.get_y() == start.get_y():
		var around_the_world :Achievement= _achievements["AROUND_THE_WORLD"]
		if around_the_world.get_level() < 1:
			around_the_world.set_level(1)
			_on_achievement_unlocked(around_the_world)
	
	if start.is_bomb_close() and not end.is_bomb() and not end.is_discovered():
		var risk_it :Achievement=  _achievements["RISK_IT"]
		if risk_it.get_level() < 1:
			risk_it.set_level(1)
			_on_achievement_unlocked(risk_it)
	
	var long_jump_level := 0
	if len(path)-2 >= 8:
		long_jump_level = 3
	elif len(path)-2 >= 5:
		long_jump_level = 2
	elif len(path)-2 >= 2:
		long_jump_level = 1
	if long_jump_level > 0:
		var long_jump :Achievement= _achievements["LONG_JUMP"]
		if long_jump_level > long_jump.get_level():
			long_jump.set_level(long_jump_level)
			_on_achievement_unlocked(long_jump)


func _on_player_started_aim(tile:TileModel, direction:ModelEnums.Directions):
	_aim_directions = [direction]
	if tile.get_direction(direction)[-1].is_beast():
		_aimed_at_fly = true


func _on_player_stopped_aim(tile:TileModel, _direction:ModelEnums.Directions):
	var think_twice :Achievement= _achievements["THINK_TWICE"]
	if think_twice.get_level() < 1:
		think_twice.set_level(1)
		_on_achievement_unlocked(think_twice)
	if tile.is_beast_close                                                   \
			and not tile.get_direction(ModelEnums.Directions.NORTH)[-1].is_beast()  \
			and not tile.get_direction(ModelEnums.Directions.SOUTH)[-1].is_beast()  \
			and not tile.get_direction(ModelEnums.Directions.WEST)[-1].is_beast()   \
			and not tile.get_direction(ModelEnums.Directions.EAST)[-1].is_beast():
		var smart_move :Achievement= _achievements["SMART_MOVE"]
		if smart_move.get_level() < 1:
			smart_move.set_level(1)
			_on_achievement_unlocked(smart_move)


func _on_player_changed_aim_direction(tile:TileModel, direction:ModelEnums.Directions):
	_aim_directions.append(direction)
	if tile.get_direction(direction)[-1].is_beast():
		_aimed_at_fly = true


func _on_player_shot(from:TileModel, to:TileModel, direction:ModelEnums.Directions):
	var shoot_path := from.get_direction(direction)
	if to.get_direction(ModelEnums.Directions.NORTH)[-1].is_beast()           \
			or to.get_direction(ModelEnums.Directions.SOUTH)[-1].is_beast()   \
			or to.get_direction(ModelEnums.Directions.WEST)[-1].is_beast()    \
			or to.get_direction(ModelEnums.Directions.EAST)[-1].is_beast():
		var near_miss :Achievement= _achievements["NEAR_MISS"]
		if near_miss.get_level() < 1:
			near_miss.set_level(1)
			_on_achievement_unlocked(near_miss)
	if to.is_beast():
		var sniper_level := 0
		var sniper :Achievement= _achievements["SNIPER"]
		if len(shoot_path)-1 >= 8:
			sniper_level = 3
		elif len(shoot_path)-1 >= 5:
			sniper_level = 2
		elif len(shoot_path)-1 >= 2:
			sniper_level = 1
		if sniper_level > sniper.get_level():
			sniper.set_level(sniper_level)
			_on_achievement_unlocked(sniper)
		
		if ModelEnums.Directions.NORTH in _aim_directions    \
				and ModelEnums.Directions.SOUTH in _aim_directions    \
				and ModelEnums.Directions.WEST in _aim_directions    \
				and ModelEnums.Directions.EAST in _aim_directions:
			var mlg :Achievement= _achievements["MLG"]
			if mlg.get_level() < 1:
				mlg.set_level(1)
				_on_achievement_unlocked(mlg)
