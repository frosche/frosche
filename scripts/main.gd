extends Control

class_name Main

signal screen_changed(scene:Enums.Scenes)
signal bad_visibility_changed(visible:bool)
signal difficulty_changed(new_difficulty:ModelEnums.Difficulties)
signal pre_load()
signal load_game()
signal seed_changed(seed:int)
signal music_volume_changed(volume:float)
signal ambient_volume_changed(volume:float)
signal effects_volume_changed(volume:float)

@export var TILE_SCENE : PackedScene
@export var TILE_S_N_TEX : Texture2D
@export var TILE_W_E_TEX : Texture2D
@export var TILE_S_E_TEX : Texture2D
@export var TILE_S_W_TEX : Texture2D
@export var TILE_N_E_TEX : Texture2D
@export var TILE_N_W_TEX : Texture2D

@onready var MAIN_NODE :Main= get_node("/root/Main")
@onready var LOAD_TIMER :Timer= get_node("/root/Main/LoadTimer")

@onready var AUDIO_MUSIC :AudioStreamPlayer= get_node("/root/Main/AudioMusic")

@onready var _gui_node :Control= get_node("/root/Main/GUI")

var _persist := Persist.new()
var _achievement_manager := AchievementManager.new(_persist)
var _achievement_buffer :Array[Dictionary]= []

var PLAYER_TEXTURE : ImageTexture

var _scene := Enums.Scenes.LOAD

var _bad_visibility := false
var _debug := false

const VISIBILITY_TIME := 5
const FADING_TIME := 2.5

const BOARD_WIDTH := 5
const BOARD_HEIGHT := 7

const WINDOW_WIDTH := 1080
const WINDOW_HEIGHT := 2340

var _game_scene : PackedScene
var _main_menu_scene : PackedScene
var _options_scene : PackedScene
var _achievements_scene : PackedScene
var _stats_scene : PackedScene
var _tutorial_scene : PackedScene

var TILE_SIZE := 128
const NUM_JUMP_SOUNDS := 9
var _jump_sounds :Array[AudioStream]= []
const NUM_TILE_TEXTURES := 48
var _tile_textures :Array[ImageTexture]= []
const NUM_TILE_BROKEN_TEXTURES := 5
var _tile_broken_textures :Array[ImageTexture]= []
var _broken_tiles_remaining := 0

var _seed := 0

func _init() -> void:
	print("  ,     ,")
	print("  (\\,-,/)")
	print("   (o o)")
	print(" ===\\_/===")
	print("   / o")
	print("  / mm\\")
	print("(_\\   /")
	print("   `\"`\"")
	print(" mi lesz?")
	
func _ready() -> void:
	assert(TILE_SCENE != null)
	assert(TILE_S_N_TEX != null)
	assert(TILE_W_E_TEX != null)
	assert(TILE_S_E_TEX != null)
	assert(TILE_S_W_TEX != null)
	assert(TILE_N_E_TEX != null)
	assert(TILE_N_W_TEX != null)
	assert(MAIN_NODE != null)
	assert(LOAD_TIMER != null)
	assert(AUDIO_MUSIC != null)
	assert(_gui_node != null)
	
	emit_signal("music_volume_changed", _persist.get_volume_music())
	emit_signal("ambient_volume_changed", _persist.get_volume_ambient())
	emit_signal("effects_volume_changed", _persist.get_volume_effects())
	
	_achievement_manager.connect("achievement_unlocked", Callable(self, "_on_achievement_unlocked"))
	
	TILE_SIZE = int(WINDOW_WIDTH*0.95 / BOARD_WIDTH)
	
	randomize()
	change_scene(Enums.Scenes.LOAD)
	emit_signal("pre_load")

func _process(_delta : float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()

func _on_play_pressed() -> void:
	change_scene(Enums.Scenes.GAME)

func _on_visibility_pressed() -> void:
	_bad_visibility = !_bad_visibility
	emit_signal("bad_visibility_changed", _bad_visibility)

func _on_difficulty_decrease() -> void:
	var diff := get_difficulty()
	if diff != ModelEnums.Difficulties.TRAINEE:
		set_difficulty((diff-1) % ModelEnums.Difficulties.size())
	emit_signal("difficulty_changed", get_difficulty())
	
func _on_difficulty_increase() -> void:
	var diff := get_difficulty()
	if diff != ModelEnums.Difficulties.HARD:
		set_difficulty((diff+1) % ModelEnums.Difficulties.size())
	emit_signal("difficulty_changed", get_difficulty())

func _on_debug_pressed() -> void:
	_debug = not _debug

func _on_seed_changed(value:float) -> void:
	_seed = int(value)
	seed_changed.emit(value)

func _on_achievement_unlocked(_id:String, achievement_name:String, level:int) -> void:
	_achievement_buffer.append({"name":achievement_name, "level":level})

func is_debug() -> bool:
	return _debug

func set_debug(debug: bool) -> void:
	_debug = debug

func get_seed() -> int:
	return _seed

func set_seed(value:int) -> void:
	_seed = value
	seed_changed.emit(value)

func get_texture(tile_type:ModelEnums.Tiles) -> Texture2D:
	match tile_type:
		ModelEnums.Tiles.NORMAL:
			return _tile_textures[randi()%NUM_TILE_TEXTURES]
		ModelEnums.Tiles.BOMB:
			if _broken_tiles_remaining == 0:
				_tile_broken_textures.shuffle()
				_broken_tiles_remaining = NUM_TILE_BROKEN_TEXTURES
			_broken_tiles_remaining -= 1
			return _tile_broken_textures[_broken_tiles_remaining]
		ModelEnums.Tiles.S_N:
			return TILE_S_N_TEX
		ModelEnums.Tiles.W_E:
			return TILE_W_E_TEX
		ModelEnums.Tiles.S_E:
			return TILE_S_E_TEX
		ModelEnums.Tiles.S_W:
			return TILE_S_W_TEX
		ModelEnums.Tiles.N_E:
			return TILE_N_E_TEX
		ModelEnums.Tiles.N_W:
			return TILE_N_W_TEX
	breakpoint
	return _tile_textures[0]

func get_jump_sound() -> Resource:
	return _jump_sounds[randi()%NUM_JUMP_SOUNDS];

func set_difficulty(diff:ModelEnums.Difficulties) -> void:
	_persist.set_difficulty(diff)

func get_difficulty() -> ModelEnums.Difficulties:
	return _persist.get_difficulty();

func change_scene(scene:Enums.Scenes) -> void:
	for i in range(0, _gui_node.get_child_count()):
		_gui_node.get_child(i).queue_free()
	var node : Control
	match scene:
		Enums.Scenes.MAIN_MENU:
			node = _main_menu_scene.instantiate()
			node.add_achievements(_achievement_buffer)
			_achievement_buffer.clear()
		Enums.Scenes.STATS:
			node = _stats_scene.instantiate()
		Enums.Scenes.GAME:
			node = _game_scene.instantiate()
			var game_model :GameModel= node.get_game_model()
			game_model.connect("game_over", Callable(_persist, "_on_game_over"))
			game_model.connect("game_started", Callable(_achievement_manager, "_on_game_started"))
			game_model.connect("game_over", Callable(_achievement_manager, "_on_game_over"))
			var player := game_model.get_player()
			player.connect("stepped", Callable(_achievement_manager, "_on_player_stepped"))
			player.connect("started_aim", Callable(_achievement_manager, "_on_player_started_aim"))
			player.connect("stopped_aim", Callable(_achievement_manager, "_on_player_stopped_aim"))
			player.connect("changed_aim_direction", Callable(_achievement_manager, "_on_player_changed_aim_direction"))
			player.connect("shot", Callable(_achievement_manager, "_on_player_shot"))
		Enums.Scenes.LOAD:
			node = load("res://scenes/ScreenLoad.tscn").instantiate()
			node.connect("loading_finished", Callable(self, "_on_loading_finished"))
		Enums.Scenes.OPTIONS:
			node = _options_scene.instantiate()
		Enums.Scenes.ACHIEVEMENTS:
			node = _achievements_scene.instantiate()
		Enums.Scenes.TUTORIAL:
			node = _tutorial_scene.instantiate()
		_:
			breakpoint
	_scene = scene
	_gui_node.add_child(node)
	emit_signal("screen_changed", _scene)


func is_scene(scene:Enums.Scenes) -> bool:
	return _scene == scene

func get_games_stats() -> Dictionary:
	return _persist.get_games_stats()

func get_games() -> Array:
	return _persist.get_games()

func reset_stats() -> void:
	_persist.reset_games_played()
	_achievement_manager.clear_stats()

func get_achievements() -> Array[Achievement]:
	return _achievement_manager.get_achievements()


func _on_music_slider_value_changed(value:float) -> void:
	assert(value >= 0.0)
	assert(value <= 1.0)
	_persist.set_volume_music(value)
	emit_signal("music_volume_changed", _persist.get_volume_music())


func _on_ambient_slider_value_changed(value:float) -> void:
	assert(value >= 0.0)
	assert(value <= 1.0)
	_persist.set_volume_ambient(value)
	emit_signal("ambient_volume_changed", _persist.get_volume_ambient())


func _on_effects_slider_value_changed(value:float) -> void:
	assert(value >= 0.0)
	assert(value <= 1.0)
	_persist.set_volume_effects(value)
	emit_signal("effects_volume_changed", _persist.get_volume_effects())

func _on_loading_finished() -> void:
	if _persist.is_tutorial_done():
		change_scene(Enums.Scenes.MAIN_MENU)
	else:
		change_scene(Enums.Scenes.TUTORIAL)

func get_music_volume() -> float:
	return _persist.get_volume_music()

func get_ambient_volume() -> float:
	return _persist.get_volume_ambient()

func get_effects_volume() -> float:
	return _persist.get_volume_effects()

func is_bad_visibility() -> bool:
	return _bad_visibility
